
function preload() { //Preload to load the json-files before loading the page
  dataQuestions = loadJSON('questions.json');
}

function setup() {
  createCanvas(1350, 750);
  // Create an Audio input
  mic = new p5.AudioIn();
  // start the Audio Input.
  mic.start();
}

function draw() {
  background("#3d6098");
  levelbar();
  businessSide();
  logoEyes();
  nextLevel();
  topUsers();
  micValue();
  makeCoinsAndStopCoins();
  // questions();

}
