
class Coin {
  constructor() {
    this.speed = random(1,2);
    this.size = (25,25);
    this.pos = new createVector(random(1015,1395),105);
  }
  move() {
    this.pos.y = this.pos.y + this.speed;
    //making the coins stop if they hit the bottom of the canvas
    if (this.pos.y >= height - this.size/2){
      this.speed = 0;
    }
  }
  show() {
    push();
    stroke('#FFDB33');
    strokeWeight(3);
    fill('#FFF333');
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
    textSize(14);
    textAlign(CENTER);
    fill('#FFDB33');
    stroke(0);
    strokeWeight(1);
    text("$", this.pos.x, this.pos.y+4.5);
    pop();
  }
  coinsTouching(other){
    let d = dist(this.pos.x,this.pos.y,other.pos.x,other.pos.y);
    if (d < this.size/3 && other.speed == 0) {
      return true;
    } else {
      return false;
    }
  }
}

function makeCoinsAndStopCoins(){
  for (let i = 0; i < coins.length; i++) { //objekter bliver ved med at tegnes
    coins[i].move();
    coins[i].show();
    for (var j = 0; j < coins.length; j++) {
      if (coins[i] !== coins[j] && coins[i].coinsTouching(coins[j])) {
        coins[i].speed = 0;
      }
    }
  }
}

function micValue() {
  let volValue = mic.getLevel();
  let mappedVol = floor(map(volValue,0,1,0,15));
  // for (let i =0; i == mappedVol; i++;)
  if (mappedVol > 2.5){
    coins.push(new Coin);
  }
  // print(vol);//vol er den værdi, som vi kan bruge i et if-statement i forhold til hvor hurtigt/hvor mange mønter der skal falde
  // if (volValue>10) {
  // }
  print(mappedVol);
}

function keyPressed() {
  keyValue++;
  if(keyValue > 1) {
    coins.push(new Coin);
    keyValue = 0;
  }
}

function mouseMoved() {
  mouseValue++;
  if(mouseValue > 10) {
    coins.push(new Coin);
    mouseValue = 0;
  }
  return false;
}

function mouseClicked(){
  coins.push(new Coin);
  return false;
}

function businessSide() {
  rect(1000, 0, 350, 750);
  if (mouseX > 1000) {
    noCursor();
  } else {
    cursor('default');
  }
}
