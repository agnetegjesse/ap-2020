let currentPoints = 40;
let level = 1;

let randomDifficulty;
let value;
let randomQuestion = [];
let paragraphs;

function logoEyes() {
  noStroke();
  fill("#FFFFFF");
  rect(0,0,1400,80);
  stroke(0);
  fill("#213159");
  textFont('Verdana');
  textAlign(CENTER);
  text("QUESTION ROOM", width/2, 62);
  textSize(50);

  movingEyeX1 = map(mouseX, 0, width, 791, 800);
  movingEyeX2 = map(mouseX, 0, width, 831, 840);
  movingEyeY = map(mouseY, 0, height, 45, 52);

  push();
  noStroke();
  fill("#213159");
  ellipse(movingEyeX1,movingEyeY,15,15);
  ellipse(movingEyeX2,movingEyeY,15,15);
  pop();

}

function questions() {
  let yPos=100;
  let paragraphs = [];
  let inputFields = [];
  let points = [];
  let buttons = [];

  //jeg har slettet let under for-loopet, fordi det ikke var nødvendingt, da det bare er definitioner, tror jeg. ændrer sig måske med next-button
  for (let i =0; i < 4; i++) {
    randomDifficulty = random([0, 1, 2, 3, 4]);
    questions = dataQuestions.allQuestions[randomDifficulty].questions;
    value = dataQuestions.allQuestions[randomDifficulty].value;
    randomQuestion = random(questions);
    paragraphs[i] = randomQuestion;
    text1 = createP(randomQuestion);
    text1.position(60,yPos*i+210)
    .style ('color',"#FFFFFF")
    .style('font-family', 'Verdana')
    .style('font-size', '13px');
    print(paragraphs[i]);

    //inputfelter
    inputFields[i] = createInput("")
    .position(60,50+yPos*i+200)
    .size(440,15)
    .value("")
    .attribute('placeholder', 'Earn x points here'.replace("x",value));

    //antal point spørgsmålet giver. Er placeret bag contribute-button
    points[i] = createP("+"+value)
    .position(540,235+i*100)
    .style ('color',"#FFFFFF")
    .style('font-family', 'Verdana')
    .style('font-size', '13px');


    //contribute buttons
    buttons[i] = createButton("CONTRIBUTE")
    .position(530,250+i*100)
    .size(95,24)
    .style('background-color', "#05004E")
    .value(value)
    .style('color', "#FFFFFF")
    .style('border', 'none')
    .style('text-align', 'center')
    .style('padding', "5px 5px")
    .style('border-radius', "8px")
    // Gemmer knappen i sin egen variabel, som ikke bliver overskrevet. Når knappen bliver trykket på, så kører den
    // Funktionen herunder. (Det eneste funktion gør, er at sende knappen videre til din rigtige funktion.
    .mousePressed(function(){ buttonPressed(buttons[i], inputFields[i], i)});

    nextButton = createButton('NEXT')
    .size(85,24)
    .style('background-color', "#05004E")
    .style('color', "#FFFFFF")
    .style("border-radius","8px")
    .style('border', 'none')
    .style('padding', "2px 2px")
    .style('font-size', '15px')
    .style('text-align', 'center')
    .position(60,630) //530
    .mousePressed(function(){ newQuestions(buttons[i], inputFields[i], paragraphs[i], points[i], i)});
    // .mousePressed(function(){ questions(buttons[i], inputFields[i], paragraphs[i], points[i], i)});
  }
}

function buttonPressed(contributeButton, inputFields, i) {
  let input = inputFields.value();
    if (input == "") {
    alert("Please type an answer");
    return false;
  }
  else {
    contributeButton.hide();
    currentPoints = (contributeButton.value())/10 + currentPoints;
    // EVT LÅS INPUT
    return true;
  }
}

function newQuestions(buttons, inputFields, paragraphs, points, i) {
  let oldParagraphs = paragraphs.splice(0);

  //print(paragraphs);

  // paragraphs0.remove();
  // paragraphs1.remove();
  // paragraphs2.remove();
  // paragraphs3.remove();
  print(oldParagraphs);
}

function topUsers() {
  let userX = 710
  let userY = 285

  // EVT GØR NOGET VED, AT DET BLIVER LOOPET OG SKREVET I FED
  push();
  textSize(22);
  fill("#05004E");
  textAlign(CENTER);
  text("TOP USERS", 820, 235);
  strokeWeight(2)
  line(720, 248, 919, 248);
  pop();

  // Top1
  // Username
  push();
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Linda Johnson", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("665", userX+130, userY);
  pop();

  // Top2
  push();
  translate(0, 40);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Robert Smith", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("597", userX+130, userY);
  pop();

  // Top3
  push();
  translate(0, 80);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Maria Rogers", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("572", userX+130, userY);
  pop();

  // Top4
  push();
  translate(0, 120);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Jane Coleman Wright", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("565", userX+130, userY);
  pop();

  // Top5
  push();
  translate(0, 160);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Martin Cook Morris", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("501", userX+130, userY);
  pop();

  // Top6
  push();
  translate(0, 200);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Martin Cook Morris", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("501", userX+130, userY);
  pop();

  // Top7
  push();
  translate(0, 240);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Martin Cook Morris", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("501", userX+130, userY);
  pop();

  // Top8
  push();
  translate(0, 280);
  // Username
  textSize(10);
  fill("#05004E");
  textAlign(LEFT);
  text("Martin Cook Morris", userX, userY);

  // Level bar
  fill("#B2DAFF");
  rect(userX+130, userY-8, 90, 7, 10);
  fill("#05004E");
  rect(userX+130, userY-5.5, 60, 2, 10);
  fill("#B2DAFF");
  ellipse(userX + 130, userY-4, 26, 17);

  // Level number
  textSize(10);
  textAlign(CENTER);
  fill("#05004E");
  text("501", userX+130, userY);
  pop();
}

function levelbar() {
  push();
  translate(-265, -70);
  fill("#B2DAFF");
  rect(width/2-200, 185, 300, 25, 15);
  fill("#05004E");
  rect(width/2-200, 191.5, currentPoints, 12, 15);
  fill("#B2DAFF");
  ellipse(width/2-200, 198, 70, 50);

  //level
  textSize(30);
  textAlign(CENTER);
  fill("#05004E");
  text(level, width/2-202, 209);
  pop();
}

function nextLevel() {
  if (currentPoints >= 290) {
    currentPoints = 40;
    level++;
    print(currentPoints);
  }
}
