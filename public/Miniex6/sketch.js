let imgCat;
let imgCity;
let imgBird;
let imgDog;
let munchSound;
let a = 255;
let score = 0;
let bird = [];
let minBird = 5;//min number of birds on the screen
let dog = [];
let minDog = 2; //min number of dogs on the screen

function preload(){
  imgCat = loadImage('pictures/cat.gif');
  imgCity = loadImage('pictures/city.jpg');
  imgBird = loadImage('pictures/bird.png');
  imgDog = loadImage('pictures/dog.gif');
  munchSound = loadSound('munch.mp3');
}
function setup() {
  createCanvas(1000,600);
  cat = new Cat (50,320,150);
  for(let i=0; i<=minBird; i++){
    bird[i] = new Bird(width+5,random(75,400),100);
  }
  for(let i=0; i<minDog; i++){
    dog[i] = new Dog(width+5,300,200);
  }
gameOver(); //The start of the game is when everything is not looping
}
function draw() {
  background(255);
  //background image of the city
  image(imgCity,0,0);
  imgCity.resize(width,500);
  //the text that the player sees when entering the game
  gameRules();
  //The movements and showing of the cat
  cat.show();
  cat.moveLeftAndRight();
  cat.jump();

  //The beams to jump on to
  noStroke();
  fill(100);
  rect(250, 275, 175, 10);
  rect(700, 275, 175, 10);

  //for loop to make all the bird show and move
  for (let i=0; i<bird.length; i++){
    bird[i].move();
    bird[i].show();
    if (dist(bird[i].x,bird[i].y, cat.x,cat.y) < 100) {
      bird.splice(i,1); //if the distance between the cat-image and bird-image is <100 make the bird disapear
      score++;
      munchSound.play();//sound when you eat a bird
      checkAnimalsNum(); //function to check the current number of animals
    } else if (bird[i].x < -50) { // if the cat doesn't catch the bird, the bird still disappears
      bird.splice(i,1);
      checkAnimalsNum();
 }
  }
  //The multiple dogs showing and moving
  for (let i=0; i<dog.length; i++) {
    dog[i].show();
    dog[i].move();
    if (dist(dog[i].x,dog[i].y, cat.x,cat.y) < 75) {
      gameOver(); //if the distance is too small - game over
    } else if (dog[i].x < -100) { // if the cat doesn't hit dog it disappears
      dog.splice(i,1);
      checkAnimalsNum();
    }
  }
}
function gameRules (){ //The gamerules which are dispayed in the beginning
  fill(255,a);
  textSize(30);
  textAlign(CENTER);
  text('Eat as many birds as possible', width/2,height/2-130);
  text('and avoid the dogs by jumping onto the beams', width/2,height/2-90);
  text('Use the arrows to control the cat',width/2,height/2-50);
  text('Press any key to start', width/2,height/2+10);
  //The score board
  fill(0);
  text('Score = '+ score, width/2, 560);
}
function keyPressed(){
  loop(); //to start the game
  a = 0; //make the rules see through
}
function gameOver (){
  fill(255);
  textSize(50);
  text('GAME OVER',width/2,height/2-50);
  text('Refresh to restart',width/2,height/2);
  noLoop(); //stops loop
}

function checkAnimalsNum() { // check the number of birds and dogs
  if (bird.length < minBird) {
    bird.push(new Bird(width+5,random(75,400),100));
    if (dog.length < minDog) {
      dog.push(new Dog(width+5,300,200));
    }
  }
}
