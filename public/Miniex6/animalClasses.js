//Below are all my different classes

//cat object is created
class Cat {
  constructor(x,y,size) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.speed = 8;
  }
  //show image of the cat-gif
  show(){
    image(imgCat,this.x,this.y);
    imgCat.resize(this.size,this.size);
  }
  moveLeftAndRight(){
    if (keyIsPressed === true && keyCode === RIGHT_ARROW && this.x<width-145){
      this.x=this.x+this.speed;
    } else if (keyIsPressed === true && keyCode === LEFT_ARROW && this.x>0) {
      this.x=this.x-this.speed;
    }
  }
  jump(){
    //the different opporitunites with the up arrow
    if (keyIsPressed && keyCode === 38 && this.x>165 && this.x<350){
      this.y=125;
    } else if (keyIsPressed && keyCode === 38 && this.x>615&&this.x<800) {
      this.y=125;
    }  if (this.x<165||this.x>350 && this.x<615||this.x>800){
      this.y=320;
    }
    //the different opporitunites with the down arrow
    if (keyIsPressed === true && keyCode === 40){
      this.y=320;
    }
  }
}
//bird object is created
class Bird {
  constructor(x,y,size) {
    this.x = x;
    this.y = y;
    this.speed = floor(random(3,8));
    this.size = size;
    this.time = frameCount;
  }
  move() {
    this.x-=this.speed; //the same as this.pos.x=this.pos.x-this.speed
  }
  show() {
    image(imgBird, this.x,this.y,this.size,this.size);
  }
}
class Dog {
  constructor(x,y,size) {
    this.x = x;
    this.y = y;
    this.speed = random(1,6);
    this.size = size;
  }
  move() {
    this.x-=this.speed; //the same as this.pos.x=this.pos.x-this.speed
  }
  show() {
    image(imgDog, this.x,this.y,this.size,this.size);
  }
}
