![ScreenShot](miniex6.PNG) 

RUNME: https://agnetegjesse.gitlab.io/ap-2020/Miniex6/

Folder with sketch and index file: https://gitlab.com/agnetegjesse/ap-2020/-/tree/master/public/Miniex6

**Describe how does your game/game objects work?**

My game is about a hungry cat that wants to eat as many birds as possible without running into any dogs. When starting the game, the rules are appearing, and the cat is big because this was my only solution to make draw stop looping. You control the cat with the arrows, and it is only possible to jump onto a beam and not everywhere in the canvas. When the cat-image is within a certain range of a bird-image, the bird-image disappears, an “eat-sound” is played and 1 is added to your score which you can see in the bottom of the canvas. If you are within a curtain distance of a dog-image, noLoop is called and “GAME OVER” is printed on the screen. 

I have three different objects in my program: a cat object which you can control, multiple bird objects which comes from right side of the canvas and moves at a random speed and are being positioned at random heights and two dog objects at the screen at the same time.  

**Describe how you program the objects and their related attributes and methods in your game.**

The “main character” is the cat, which the user controls. I have programmed this to move with a speed of 8 pixels every time you press the left or right button. The cat can also jump onto the beams. This was kind of hard to program because I had to define where exactly the beams were positioned in order to only make the cat able to respond to the up-arrow when positioned under the beams and then also make it move down to the “ground” when you move beyond the beams. Also, when the cat “eats” a bird it says an eating sound like if you are taking a bite of something. I felt like this was another attribute that my cat object had to contain. So, what my can does is show, eat, sound like it is eating, move from left to right and jump within a curtain field. 

The dog and bird classes have some of the same attributes which are to move at a random speed within a range and both produce new objects when the get to the other end of the canvas. Here they also disappear in order to make me able to count how many there are on the screen right now in order to produce new. 

I had to make a connection between the cat object and the dog objects and the bird objects in order to create interaction. This was very difficult because the variables inside a class cannot be accessed outside the class itself. I had to make a lot of changes to my program and add properties to my constructor in order to having these variables available outside the class. These relations between objects are central in object orientated programming. When dealing with objects there are relations between objects and subjects which is both human-computer relations and non-human relations and these relations are central. (from Winnies mini-lecture in class). 

**What are the characteristics of object-oriented programming and the wider implications of abstraction? Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?**

The characteristics of this is that you (as a programmer) think of something as an object which has different attributes, looks, movements, things it does etc. which can be manipulated. 

You can encapsulate different aspects of what it means to be that exact object – in my case a cat. I have defined how the cat must look, what it can do, how it sounds and how it moves. This concrete model is made from my own point of view and is all about how I perceive cats and simplify the objects complexity into something I am able to code. Therefore, this object is not neutral – an object is never neutral. Every decision about how big, small, fast or slow an object moves is made by someone. They can decide what defines or constitutes a curtain object and quantify its characteristics. Everything can be an object: humans, circles, cats etc. But what defines an object and how is this coded? Is it only what is defined inside the computer? No, there are a lot of other things, and we must think critically about how an objects complexity is simplified and turned into a concrete model. 

We had a nice talk about this during the last instructor lesson, where Ann told us about her master thesis and how she relates object orientated programming and virtual objects or characters (e.g. in games) to how we perceive our reality and shape our sense of what is normal. This has made me think about how you e.g. make an avatar in an online game. You only have curtain things to choose from, maybe predefines skin colors, genders, outfits etc. 

How are these things programmed and why can e.g. the player not have any more influence on how the character can move, look or sound? Maybe this is also a question of security of object encapsulation where you have the opportunity to hide the data in order to minimize risk of errors and keep the user in a sort of “black box” where you don’t have to think about what actually goes on behind the screen but instead just do the things that make the program work. 


