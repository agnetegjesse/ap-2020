**Describe your program and what you have used and learned**

My program contains two different emojis which are both manipulative in different ways. The emoji on the left is supposed to look just like an ordinary emoji, but it lacks 1/4 of its left eye. This is supposed to make the user curious and hopefully this makes the user discover the slider in the bottom of the canvas. At first sight the slider controls the position of the emoji's mouth, but by inspecting it further it also controls the direction in which the eyes are turning. When you make a "happy" emoji, the eyes turn counter clock wise, and if you make a "sad" emoji the eyes turn clock wise. It took me quite a long time to figure out how to make the mouth move up and down, and I have tried out a bunch of difefrent functions like arc() and ellipse(). But I couldn't get any of these to work, so instead I found a reference called curveVertex() at the referencepage: https://p5js.org/reference/#/p5/curveVertex. I made to middle point variable and made conditions and then it worked with the slider which i found here https://p5js.org/reference/#/p5/createSlider. I would have liked it to function automatically, so that I didn't need any slider, but after many attemps i gave up. 

The other emoji just consists of two points and a slightly more dark yellow text where it says "draw me". It is now up to the individual user to choose what kind of emoji, the person wants. I does not have to be limited to a face, but the two points should lead the thought to a couple of eyes. I learned that I had to set a limit for where you could draw because otherwise there would be drawing all over the slider as well, so i learned that mouseX is another contition that you can make in you if statement. 

**How would you put your emoji into a wider cultural context that concerns represenation, identity, race, social, economics, culture, device politics and beyond?**

I focus very much on what kinds of emojies that lacked. At first i wanted to make a mood changing emoji and an undefined emoji, and I ended up doing something in that direction. The draw me emoji is supposed to break away from the ordinary emojies with curtain expressions, looks, colors ect. It gives the user the oppritunity to represent him- og herself. You can make the emoji fat, give it glasses, make an animal, patch its eye, and give it whatever facial expression you feel like. I have chosen to think about this emoji in relation to the statement that we live in a post racial and post gender time from "modifing the universal". This emoji is shapeless and colorless and the user has every possibility to represent him- or herself. 

The drag me emoji also comes from the thought of lack of representation and taboo around stress, depression and the perfectness culture in our societiy. Eventhough you are very busy and can't hold your thoughts together you smile. The spinning eyes are representing all the things that goes on inside of our mind that noone can se. This feeling is no represented in any emoji now and it is very hard to seee in other people and show to other pepole. 

https://agnetegjesse.gitlab.io/ap-2020/Miniex2/

https://gitlab.com/agnetegjesse/ap-2020/-/blob/master/public/Miniex2/index.html

![ScreenShot](miniex2.PNG)



