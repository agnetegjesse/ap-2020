let slider;
let eye=0;
function setup() {
  slider = createSlider(250, 340, 290);
  slider.position(160,450);
  slider.style('width', '80px');
  createCanvas(800,500);
  background('lightyellow');
}

function draw() {
  //emoji 1 on the left
  noStroke();
  fill('yellow');
  ellipse(200,250,250,250);

  //text drag me
  noStroke();
  fill('yellow')
  textSize(32);
  text('DRAG ME',125,430)

//eyes
  fill(0);
  arc(150, 210, 30, 30, radians(90), radians(eye));
  arc(250, 210, 30, 30, radians(eye), radians(360));

  //mouth
  let mouth = slider.value();
  noFill();
  strokeWeight(3);
  stroke(0);
  beginShape();
  curveVertex(125, 290);
  curveVertex(125, 290);
  curveVertex(205, mouth);
  curveVertex(275, 290);
  curveVertex(275, 290);
  endShape();

  if (mouth<290){
    eye=eye+1
  }else if (mouth>290) {
    eye=eye-1
  }

  //emoji 2 on the right
  //text "draw me"
  noStroke();
  fill('yellow')
  textSize(32);
  text('DRAW ME',520,430)

  //eyes size, color and place
  strokeWeight(30);
  stroke(0);
  point(550,210);
  point(650,210);

//drawing function
  if (mouseIsPressed && mouseX>400) {
    strokeWeight(5)
    line(mouseX, mouseY, pmouseX, pmouseY);
  }
}
