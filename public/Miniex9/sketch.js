var api = "https://api.giphy.com/v1/gifs/search?";
var apiKey = "&api_key=qrQpNgaVmF9ekrR6SGK3Ca5fFt4Qd83x";
var queryGlad = "&q=happy";
var querySad = "&q=sad";
let buttonGlad;
let buttonSad;
let gladSong;
let sadSong;

function preload(){
  //sound effects are loaded when you press the buttons
  gladSong = loadSound('gladSong.mp3');
  sadSong = loadSound('sadSong.mp3');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  textSize(32);
  fill(0);
  textAlign(CENTER);
  text('How are you feeling?', width/2, 50);

// CSS styling for sad button
  buttonGlad = createButton('GLAD');
  buttonGlad.style('background-color', 'pink');
  buttonGlad.style("border-radius","12px");
  buttonGlad.style('border', 'pink')
  buttonGlad.style('padding', "10px 14px")
  buttonGlad.style('font-size', '20px');
  buttonGlad.position(width/2-80, 100);
  buttonGlad.mousePressed(giphyGlad);

// CSS styling for sad button
  buttonSad = createButton('SAD');
  buttonSad.style('background-color', 'grey');
  buttonSad.style("border-radius","12px");
  buttonSad.style('border', 'grey')
  buttonSad.style('padding', "10px 20px")
  buttonSad.style('font-size', '20px');
  buttonSad.position(width/2+20, 100);
  buttonSad.mousePressed(giphySad);
}

//this is what happes when you press the glad button
function giphyGlad() {
  //the url is searching for glad
  urlGlad = api + apiKey + queryGlad;
  loadJSON(urlGlad, gotGladData);
  //glad sound restarts when button is pressed
  if (sadSong.isPlaying()) {
    sadSong.stop();
  }
    gladSong.playMode('restart');
    gladSong.play();
}

//this is what happes when you press the sad button
function giphySad() {
  //the url is searching for sad
  urlSad = api + apiKey + querySad;
  loadJSON(urlSad, gotSadData);
  //sad sound restarts when button is pressed
  sadSong.play();
  if (gladSong.isPlaying()) {
        gladSong.stop();
  }
  sadSong.playMode('restart');
  sadSong.play();
}
//these two next functions retrieve the data within the JSON data
function gotGladData(gladGiphy) {
  i = floor(random(gladGiphy.data.length)); // variable to choose a random gif within JSON file
  gif1 = createImg(gladGiphy.data[i].images.original.url, ""); // path to choose the url of the gif
  gif1.position(width/2-325, 200);
  gif1.size(650, 500);
}

function gotSadData(sadGiphy) {
  j = floor(random(sadGiphy.data.length)); //variable to choose a random gif within JSON file
  gif2 = createImg(sadGiphy.data[j].images.original.url, ""); // path to choose the url of the gif
  gif2.position(width/2-325, 200);
  gif2.size(650, 500);
 }
