

![ScreenShot](miniex9.PNG) 


RUNME: https://agnetegjesse.gitlab.io/ap-2020/Miniex9/ - remember sound on ;) 

Folder with sketch and index file: https://gitlab.com/agnetegjesse/ap-2020/-/tree/master/public/Miniex9

<h2>Your Binary Emotions</h2>

For this miniEx I have worked with: @freyjavm @OliviaSP @StineMygind

**What is the program about? which API have you used and why?**

We have created a program where you are asked how you are feeling. You only have the possibility of answering “Glad” or “Sad”. When you hit one of the buttons your “feeling” is represented by a Gif. We have chosen to use a search API from GIPHY, as we had multiple problems with loads of other API’s - GIPHY API opposedly was really easy to connect to and use. It is somewhat limited what you can do with this API, but as gifs are fun to look at, it is an easy way of putting a comic sense into your program. 

Our aim is to display the ironic binarity in the concept of computing feelings through showing gifs which are “feeling the same things” as you. Therefore this API was perfect for that purpose. 

When you use the API it only gives you ten gifs on the json file, which is also interesting to reflect upon when talking about limitations when working with API and asymmetric protogological exchange.

Our intentions behind this program was to illustrate that computing is built up upon binary code, and when using a computer it is not possible to show all of your emotions. We wanted to illustrate this by only making two choices; glad or sad that emphasizes the binary code which computers consist of and which we have been critical about in our reflections this week. The computer is built up by different functions from a binary level inside the computer to a visual level where the user is interacting with the computer, and sometimes the user has many options and sometimes he/she doesn’t, but he/she can never choose something in between the options presented. A computer is simpler and more limited than a feeling person, because it is limited by a human making decisions and modifying the world. The limitations and decisions are made by programmers/companies, so they are the ones in charge of the possibilities, just like they have charge over APIs. 

We think that these thoughts behind our program, links very well to the text “Modifying the Universal”, where Femke Snelting is talking about that when you modify something that has to be universal, there will always be complications and further modifications to make. We have very much used her thoughts about this in our program. On Facebook we have the possibility to react in six different ways on posts or messages, which has recently been modified from just liking. This way of reacting has changed the way people interact on Facebook and still is a very limited way of communicating and expressing feelings, which is very much controlled by the developers of Facebook. 

**Can you describe and reflect on your process of making this mini exercise in terms of acquiring, processing, using and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the selected data? What are the power-relations in the chosen APIs? What is the significance of APIs in digital culture?**

We started our process by trying to use different APIs, but it turned out to be a difficult task. At first we looked up API’s for beginners to get an API that would be helpful where we are in our process of learning how to use API’s. Lists on Google would generally have Twitter’s API on the top and therefore we started trying to get an API key from that. In doing this we went through a lot of time consuming steps to just get an API key. The steps consisted of different questions which were concerned with the usage of the API and should be responded to by answers with at least 100-200 characters. This made us reflect upon the accessibility of data which we, in this case, found as a very difficult process and at the same time an eyeopener to how the relationship between the company and user is very clear. In the text "API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange” by Eric Snodgrass and Winnie Soon, it is mentioned that “an API can be seen as both an entry point into the black box of a particular computational service, but also as a clearly demarcated barrier towards other possible exchanges with this service” (p.3) and this also points to the power relationship between user and company. The company, Twitter in this case, has decided which opportunities the user should have and in their considerations they have at the same time found out what they want from the user.

After not being able to use twitter’s API, we decided to try others (with the help from Shiffman), which we also had loads of trouble with, as the sites had changed since Shiffman created his videos. This made us reflect upon that API developer sites had changed a little since only 2015 to sites that gave less info and seemed more addressed to people with further skills. So, even though we wanted to create our concept before choosing an API, we ended up choosing the API from GIPHY, as it was the easiest one. GIPHY has different endpoints and we chose the search endpoint which is allowing us to search for gifs, but with the limitation of showing 25 gifs at a time.

APIs make it possible for third parties to use the data from a given platform, just like we have used gifs made by others. The data, which could be tweets or gifs, can be used without the acceptance from the person creating the data or any awareness that a third party is using the data somewhere else. In our example this would be that somebody uploads a gif on GIPHY and then we take the advantage of that and use it on our website without notifying this person or giving credit.

**Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time.**

It could be interesting to investigate how companies use the data that we generate through the use of APIs and how much it is worth to them. 
