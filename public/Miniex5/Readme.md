![ScreenShot](miniex5.PNG) 
 
 Rumne: https://agnetegjesse.gitlab.io/ap-2020/Miniex5/
 
 Folder with sketch- and index file: https://gitlab.com/agnetegjesse/ap-2020/-/tree/master/public/Miniex5
 
**What is the concept of your work? What is the departure point? What do you want to express? What have you changed and why?**

My concept is a throbber where the viewer can be surprised and interact with what he or she sees. 
I wanted to develop my throbber and use almost all the different kinds of syntax that has been introduced in class. I was very inspired by Asterisk Painting by John P. Bell which we worked with in a previous class in an edited version by Winnie Soon (https://editor.p5js.org/siusoon/sketches/YAk1ZCieC). 

My departure point was my miniEx3 where I made a circle rotate around a point in the center of the canvas at an irregular speed. I wanted to introduce for loops and experiment with arrays and “automate” the shifting placement of the throbber being drawn. At the same time, I wanted to incorporate an interactive element and I decided that it should be a DOM-element to practice the styling and formatting of these. 

The ellipses in the throbber are drawn in an irregular speed (the angle is increased with a random number between 0-50) and the ellipses drawn are also drawn with a random diameter. I wanted to make it look festive and like confetti in order to make a throbber that entertains the viewer and gives them something pretty to look at instead of an ordinary throbber. The many elements of randomness are to give the viewer an opportunity to be surprised and curious, and nothing ever repeats. This unpredictableness is not what you normally expect when encountering a throbber where the speed, shape and color are always repeating. 

I have added many new elements to gain more insight into different ways of using and combining the different types of syntax and learning more about the opportunities that p5.js contains. I really learnt a lot about for loop and how to combine it with arrays because I used a lot of time to try to understand the code the I link to above. 


**Reflect upon what Aesthetic Programming might be**

When I started the course “aesthetic programming”, I had a hard time understanding why we needed to learn programming if we weren’t going to be programmers but designers. In the first lesson where we were asked to reflect upon why we needed to learn programming I wrote: “To develop something as a digital designer, you need to have an understanding of how software functions.” Through the lessons my perception has developed a lot. I now think of aesthetic programming as an ever-expanding learning and exploration tool trough which one can explore different aspects of software and digital culture. 

Aesthetic programming helps one understand the basic thinking of coding which I think is crucial to demystify programming and software in general. Otherwise the field of software seems very esoteric to the broad public and the power of software development and adaption is in the hands of a small group of experts. This particular issue is addressed in the text be Vee where she argues that programming should be considered as a form of literacy. 

In the text Sylvia Scribner names three major metaphors that drive literacy promotion: literacy as adaption, as power and as a state of grace. Software surrounds us all and digital culture is a whole new dimension of our present culture. But the development in the understanding of literacy doesn’t follow this change. To think through code, understand the specific selections in a program and to be able to think critically about how it affects us in many different aspects of our lives, is what I consider to be the fundamental purpose of aesthetic programming. By considering programming as a literacy it is possible to make this way of thinking about the surrounding technology broader. 

Instead of just accepting terms and conditions, buying the newest iPhone (which is close sourced) and having no influence on the future of digital culture, people can be empowered and learn new ways of thinking and reflecting upon software, digitalization and perhaps datafication. I know my own use of technology has become much more critical and with a deeper understanding of how things actually function if you press something, use a keyboard and move your mouse. To me, the “black box” of computation has been opened at least a bit and made my aware of the importance of thinking about why, how and by whom software is developed. 
