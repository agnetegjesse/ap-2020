var angle=0;
var vent; //wait
var xPos=[1,2,3,4];
var yPos=[1,2,3];
var xCtr=0;
var yCtr=0;
var button1;
var button2;
var speed=20; //the initial framerate
var dia=25;

function setup() {
  createCanvas(600,500);
  angleMode(DEGREES);
  //creating the first button which makes the throbber move faster
  button1=createButton('FASTER');
  button1.position(width/2-165,450);
  //calling the function goFast when the button is pressed
  button1.mousePressed(goFast);
  //button styling
  button1.style("border-radius","30px");
  button1.style("font-weight","bold");
  button1.style('background','#2299fc');
  button1.style('border','#2299fc');
  button1.style('padding',"10px 14px");
  button1.style("font-size","1em");

  //creating the second button which makes the throbber move slower
  button2=createButton('SLOWER');
  button2.position(width/2+75,450);
  //calling the function goSlow when the button is pressed
  button2.mousePressed(goSlow);
  //button styling
  button2.style("border-radius","30px");
  button2.style("font-weight","bold");
  button2.style('background','#bee1fc');
  button2.style('border','#bee1fc');
  button2.style('padding',"10px 14px");
  button2.style("font-size","1em");

  //for loop taken from https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/public/sketch03/sketch03a.js
  //to make the throbber change position along the x and y axis according to the array that i made
  for (let i=0; i<xPos.length; i++){
    xPos[i]=xPos[i]*(width/(xPos.length+1));
  }
  for (let i=0; i<yPos.length; i++){
    yPos[i]=yPos[i]*(height/(yPos.length+1));
  }
}

function draw() {
  //setting the framerate and letting it be variable to make the buttons able to change it
  frameRate(speed);
  //calling my function
  drawRotateThrobber(30,30);
}

function drawRotateThrobber(x,y){
  //The rotating throbber
  push();
  noStroke();
  fill(random(0,255),random(50,200),random(100,255));
  //to make the ellipse rotate around the center of the new x and y position
  translate(xPos[xCtr],yPos[yCtr]); // xCtr and yCtr is always 0 which makes the trobber start at the first place in the array
  rotate(angle);
  ellipse(x,y,random(dia),random(dia));
  pop();
  //to make the ellipse rotate at a random speed from 0-50
  angle=angle+random(50);

  //reset the number of degrees to 0 if the angle is bigger than 360
  if (angle>360) {
    angle=0;
    //The placement of the throbber is increased by 1 every time the throbber is 360
    xCtr++;

    //if the end of the xPos array is reached, start on a new y-row (yCtr++)
    if (xCtr>=xPos.length) {
      //set the array back to 0, which means that it starts closest to the left side of the screen
      xCtr=0;
      yCtr++;

      //if the end of the yPos array is reached, start on a new x-row (xCtr++)
      if (yCtr>=yPos.length) {
        yCtr=0;
        //set the background to white with an alpha when the throbber reaches the buttom right corner
        background(255,210);

      }
    }
  }
}

//function that activates when the mouse Faster is pressed, which makes the framerate increase by 6
function goFast() {
  speed=speed+6;
}

//function that activates when the mouse Slower is pressed, which makes the framerate decrease by 6
function goSlow() {
  speed=speed-6;
}
