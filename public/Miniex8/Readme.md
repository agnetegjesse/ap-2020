![ScreenShot](miniX8.png)

**Here is a link to my and Freyja's miniEx8, which is on her profile:** https://gitlab.com/freyjavm/ap2020/-/tree/master/public/MiniX8 

I would have put it here, but couldn't get it to work. So the sketch, runme and JSON is on her profile.  

**Provide a title of your work and a short description of the work (within 1000 characters).**

We have made a poem generator that generates three short lines of random words. The form is inspired by Haiku poems, but it does not strictly
have the form of 5-7-5 syllables - sometimes it is and sometimes it is not. We started looking at how haiku poems could be created syntax wise
and then we made a form inspired by this, consisting of different world classes as shown below:

Form:
- Adjective, setting
- Conjunction, adverb, verb
- Preposition, the, noun

All the seven different word classes are stored in a JSON file. The title of our work  is “The meaning of your life” and the meaning behind
this is very ironic, as the “meaning” that is printed is very silly and most of the time it does not make any sense. This is exactly what we
want to express - that life sometimes does not always make sense and that existential questions like “what is the meaning of life?”, should
not be taken seriously. Should there be a meaning of life? Who knows and who can tell you? Do you really need a general point of life or isn’t
it enough just to be happy, have fun and follow your own path?

**Describe your program in terms of how it works, and what you have used and learnt?**

Our program is based on a JSON-file with different words in different word classes which is then called in our sketch-file. As mentioned above
we have created a frame for our poem with different words in a curtain order. This order is what creates the poems and extracts just the kind
of word that we wanted at the specific position in the poem. The program extracts a random word from the called word class and when framCount
reaches 300, it resets frameCount to 0 and does the same thing again.

The JSON-file format was very helpful to store all these different classed words and keeping our sketch-file very neat. We lent the format of
setting a JSON-file up from the Corpora-project. But even though we used this format as a starting point, we still had some troubles
creating the right path to extract certain words but managed in the end.

In the actual code, we have also played with double coding and naming our variables in a meaningful way in relation to the generated text, so
the source code becomes just as important for the meaning of the art piece as what is being generated.

**Analyze and articulate your work:**

*Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/
language) in relation to code and language.*

In the text “The Aesthetics of Generative Code” by Goeff Cox et al his main point is that code should be sensed to fully grasp the aesthetic
experience, because the code and the execution should not be separated but rather be seen as a whole. This way of thinking coding and
programming we have very much put into the way we have created our program, as to understand our message it is further emphasized when seeing
the program running and when you see how the code is built, how we name our variables and so on. He also talks about how code should play with
language and that code can have various forms to execute the same. We started writing our code the conventional way we’ve learned to write
syntax and after we got the execution right, we started playing with the “voice” of the code - the way the code itself could underline the
meaning of our program.

*How would you reflect upon various layers of voices and/or the performativity of code in your program?*

When making the program, we focused on making something which was double coded - which means that the source code has two purposes: namely
instructions to the computer and expressing something else for the human reader. We were very much inspired by the text “Speaking Code: Vocable
Code” by Geoff Cox et al. In the following citation Cox et al. introduces poetic possibilities for code beyond the functionality: “The analogy
to poetry suggests numerous aesthetic and critical possibilities for code, beyond its serving simply as functional instructions.” Geoff Cox et
al., p. 17.

This poetic performativity of code language is something which we have tried to examine when making our program. The different variables
related to changing the different words in the poems does not only perform a functional instruction for the computer, but also means something
to the reader. We wanted our variables to not just function as a “service announcement ” to make it easier to read our code, but instead
function as an extension of our poem generator which is supposed to underline these points - but they are up for interpretation:

- The names of the variables is to make the reader read the code in a different way, that he/she normally would. When reading the code aloud line
by line, you get another sense of the variables than if you just read the variable names.
- At the same time we wanted to make the code confusing without any immediate order. This is to extend the meaning of our poems - maybe there is
no direct or right path in life - instead let us stop taking everything so seriously. 
- The variables are also continuing the title of the work.
This is to make the reader continuously reflect upon the way these variables are implemented in the code and how “silly” and “non-serious”
everything can be.

Furthermore we have incorporated randomness in the program which also can be reflected upon the expression and message of the program. In “10
PRINT CHR$(205.5+RND(1)); : GOTO 10” Montford presents how random numbers are actually pseudorandom, which is the reason we have incorporated
pseudorandomness directly in the code. We for example we wrote:

let everythingIsRandom = pseudoRandom.randomWords;

We wrote this to express that even though it sometimes feels like you have no control of the meaning of your own life, everything happens for
a reason.