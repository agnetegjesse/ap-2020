var mic;
var vol;
var button;

function setup() {
  createCanvas(windowWidth, windowHeight);
  mic = new p5.AudioIn();
  mic.start();
  //frameRate is increased in order to make the line of mouse movements more coherent
  frameRate(150);
  background(255);
  //The block data capture button and styling
  button = createButton('Block data capture');
  button.position(width/2-48, height/1.1);
  button.mousePressed(noBG);
  button.style("font-size","0.4em");
  //The accept button and styling
  button = createButton('I ACCEPT');
  button.style("font-size","1.8em");
  button.center();
  button.mousePressed(newButton);

}
function draw() {
  //ellipse created to track your mouse movements. When you click the mouse a bigger ellise will be created
  noStroke();
  fill(13, 255, 0, 100);
  if (mouseIsPressed) {
    ellipse(mouseX, mouseY, 50, 50);
  } else {
    ellipse(mouseX, mouseY, 10, 10);
  }
  //The color of the text is mapped to the mic level and text is displayed all over the screen
  vol = mic.getLevel();
  var colorText = map(vol, 0, 0.4, 255, 0); //The max input is set at 0.4 in order to make the text darker easier
  for (var x = 0; x < width; x+=130) {
    for (var y = 10; y < height; y+=11) {
      fill(colorText);
      text("your data is everywhere", x, y);
    }
  }
}
//this function draws a white rectangle on top of the canvas when you press the block button
function noBG(){
  fill(255);
  rect(0, 0, width, height);
}

//this function creates a new "block data capture button" every time the accept button is pressed
function newButton(){
  button = createButton('Block data capture');
  button.position(500/random(5), 500/random(5));
  button.mousePressed(noBG);
  button.style("font-size","0.4em");
}
