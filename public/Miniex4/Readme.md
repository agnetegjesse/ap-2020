RUNME: https://agnetegjesse.gitlab.io/ap-2020/Miniex4/ - because the program captures microphone input, I found that it worked best in firefox when I opened via the link. 

Folder with sketch- and index file: https://gitlab.com/agnetegjesse/ap-2020/-/tree/master/public/Miniex4

**Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.**

*Your (in)visible tracks*

I think that one of the most remarkable things about this datafication is that the individual person doesn’t really have a choice in whether to participate in being quantified or not. I was very fascinated by the “accept”/”agree”/”consent”-buttons that pops up on almost every web page which you as user blindly press without really thinking about what kind of data you have just consented to being captured in our datafied society. My work critically illustrates how an “accept”-button is easy to press and the most eye catching, almost magnetic thing on a webpage and how blindly pressing this button leads you with a huge number of consents all over the internet, that is almost impossible to keep track of. Which data are being captured? How are they being used? And how can you delete your tracks? These are some of the things that my program aims to make the user think about. 

**Describe your program and what you have used and learnt**

In this miniEx I have chosen to capture the data from mouse movements and interactions and the microphone in the computer. I have captured the mouse data by drawing small ellipses at every mouseX and mouseY and because the background is in setup, these ellipses form a track of the mouse movements. When you click the mouse, it will be marked with a bigger ellipse in the same color. My intention with doing this was to make it visible to the viewer, how they can be tracked on a webpage when you accept cookies. 

Via the microphone the viewer’s noise and sounds are being tracked. The volume of the microphone input determines the color of the text on the white background. The text says, “your data is everywhere” and is white when there is no sound. Therefore, the viewer might not even notice the text because it blends in with the white background. The reason why I chose this, was that I reflected upon what my relation to data capture is and when I encounter it, and I realized that I don’t know anything about how I am being tracked and on which webpages what happens. This is underlined by the invisibility and the repetitive text, which reminds the viewer that your data is spread everywhere, and you don’t see it unless you start to reflect upon your tracks.  

Syntax wise, I have focused on understanding how to implement different elements like buttons, sound and my own functions and how these can be combined. Besides that, I have experimented with for loops and figured out, that it is a smart tool for making patterns or repetitive things. I think I could have used a for loop for making the new buttons, but I couldn’t make sense of how I could be done. 

**Articulate how your program and thinking address the theme 'capture all'. What are the cultural implications of data capture?**

I chose that my title should be “Your (in)visible tracks” because you normally don’t see or know how your data is being captured, and a lot of people don’t give second thoughts before pressing the button where you accept cookies. I considered the role of the button very much, when I made my program. The “I accept”-button is significantly bigger than the one small “block data capture”-button at the bottom of the screen, and the attraction of the big button (Soon text) makes us almost automatically press it. When doing this, it creates a new small button, but between these different buttons, your mouse movements are being displayed and maybe different sounds display the background text. All this is to show that even though you might not accept on one page, your data is still being captured through a lot of other canals where you have consented ex. through a sign-up. When you press the easy and obvious “I accept”-button you only make it harder for yourself to get an overview of how far your data is spread and what you can do to stop it from spreading. 

We are all constantly contributing to a growing database of data, and this contribution is more or less conscious at different people. Hopefully my program can make people reflect upon the types of footprints that they leave behind and how their own lives are being turned into something quantified that can be translated into zeros and ones.  


![ScreenShot](miniex4.PNG)
