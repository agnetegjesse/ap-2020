<h2>MiniEx10 - flowcharts </h2>

<h4> My individual flowchart</h4>
Here is a link to the folder of the game (miniex6), where you can find the sketch file and a link to the actual program so it might be easier to understand what the flowchart is explaining: https://gitlab.com/agnetegjesse/ap-2020/-/tree/master/public/Miniex6

![Flowchart](FlowchartGame.jpg)

**What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?**

This question is addressed throughout the whole readme. 

The program which I chose was my game, which has a lot of conditional logic and algorithmic procedures which I think is quite complex. Earlier, I have actually tried to explain to my mom how the program works, and she knows absolutely nothing about programming, and she didn’t understand a word I said.  

I saw this task as a way of being able to communicate the complexity and flow of the program to someone like her. This meant that everything had to be nontechnical terms and the algorithmic procedures had to be simplified but still shown as they are central to the function of the program. This was the biggest challenge and I realized that I had to go back into the code and figure out what was important to understand how the game worked. 

<h4> Our first group flowchart</h4>
(The following is made collaboratively with the rest of my group)

Here is a link to the flowchart of the first idea: https://zwz5sk.axshare.com/#id=b0fulb&p=page_1

Our first flowchart is made upon an idea concerning the theme data capture. The idea is that you open the program and see four questions (with buttons named “contribute”), a bar that shows what level you are on and a next button. The questions appearing are easy to answer, but when continuing they get more personal. The level bar’s increment depends on how many questions the user has answered and the questions themselves as they have a value depending on level of being personal.

There is still space to further develop the idea, as we want to program that “ads”, about friends answering the questions, pop up to engage the user to answer more questions.

<h4> Our second group flowchart</h4>
(The following is made collaboratively with the rest of my group)

Here is a link to the flowchart of the second idea: https://gip2go.axshare.com/#id=05flvk&p=page_1

Our second flowchart represents a program which illustrates two understandings/perceptions of data capture. At first the user sees the positive side of data capture, which is illustrated by pieces of texts and interactive elements. The user is able to “flip” or change the canvas, and the user now sees the negative side of data capture. Our intention is to show that users of the internet are automatically dragged into the capitalist illusion of data capture positivity, but have to actively search for the negative effects of data capture which are normally hidden.

**What are the technical challenges for the two ideas and how are you going to address them?**

(Because we made the flowcharts collaboratively, we also made this part of the readme together beacuse we shared our thoughts and ideas and reflected upon these toghether whilst making the flowcharts)

There are several technical challenges in our two programs. We assume that it will be very difficult to make the eyes which follow either the text the user writes (idea with questions/ranking) or the mouse (idea with the back side of the con). Furthermore, we have many elements in both ideas which are time consuming, difficult to make and need to function together. We have a logical understanding of how the programs should work technically, but we fear that there might be something we have overlooked. Because we aren’t sure of which problems we will meet yet, we think will address them through trial and error and use the reference in p5.js, Daniel Shiffman and other things on the internet to solve the challenges as they occur.

**What is the value of the individual and the group flowchart that you have produced?**

(Because we made the flowcharts collaboratively, we also made this part of the readme together and shared our thoughts and ideas and reflected upon these toghether whilst making the flowcharts)

In the group, we have made our flowcharts so they makes sense to ourselves. We used flowcharting as a way of communicating conceptual and technical aspects of our program to reach a common understanding. 

Right now the flowchart is a sketch and it only consists of the basic concept, and it could be improved and specified in many different ways. It is therefore not a blueprint, but a starting point from where our communication and programming can take place and develop. Hopefully, the flowchart can also give us a place where we can always return to, in order to grasp our conceptual thoughts and see how they have been connected to our technical thoughts. Our concept and ideas were developed before diving into the flowcharting and therefore it reflects our conceptual thoughts and what we value as the most important elements. 

The approach we have had in making our two ideas as flowchart is that it should be a help for us as a group to develop our ideas and to concretize how we wanted the program to function. It helped us to find a coherence between our individual ideas and conception of the program. The intention behind our flowcharts is to get closer to thoughts about how the program should function technically. This is why we still have some programming terms implemented, as we saw it relevant later in the process of making our final project. 

Contrary, when making my own flowchart I had already made the program and the flowchart had a value of reflection upon how to communicate the complexity of a program to a person with no programming knowledge. Also, it made me reflect upon how the program actually works and think about what algorithmic procedures are in the program. This has actually made it much more clear to me, what algorithms are and how they work. Up until the point of making this flowchart, I was still very unsure what an algorithm was and I was sure that I would never be able to program any, but when revisiting my earlier miniEx, I see that algorithmic procedures are in the conditional logic and it doesn’t have to be that hard or complicated – and it is actually possible to explain this “black box”-thing to people who does not understand programming at all. 