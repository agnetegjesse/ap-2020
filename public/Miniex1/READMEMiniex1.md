**Questions to think about in your README:**

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
I really liked my first coding experince. It was fun to get to experiment and try a lot of different things. I tried much more different things from the P5.js reference which I couldn't get to work because I didn't quite understand the different arguments. Instead I focused on understanding different 2D-shapes and how the coordinates and other arguments affected the output. When coding I experienced a lot of frustration because I could sense that there was much easier ways to do ex. the raindrops without having to copy everything and change the values manually. But I am looking forward to learning more and figuring out all these different factors. 

**How is the coding process different from, or similar to, reading and writing text?**
In general I think that the coding process is very different from writing. The importance of the syntax has surprised me a lot - especially when dealing with the libraries and capital letters. It has shown to me, that everything has to be done completely correct in order to make a succesfull program because computers are "stupid" and can't interpret anything without correct syntax. A lot of the words makes sense because the word are the same as in everyday language - ex. background, color, ellipse. 

**What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?**
After having had my first experince with coding I have gained a lot more insight in how software actually works and I already feel much more competent to talk about software. Software surrounds us everywhere and I think that the need for learning and understanding these technologies is crucial to everyone and should be considered a kind of literacy. 

 https://agnetegjesse.gitlab.io/ap-2020/Miniex1/
 I couldn't get my link to work, but the code is inside my miniEx1 folder and is called sketch.js
 
 ![ScreenShot](miniex1.PNG)