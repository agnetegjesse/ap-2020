Conditional logic
boolean expression -- true (1) or false (0)

if (boolean expression) {
executes if true
}

the trick is that we can put varibles in here
and if it is true the if statement will execute
