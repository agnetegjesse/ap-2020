let addColorR = 145;
let addColorG = 183;
let addColorB = 217;
let timeVal;
let højde = 320;
let tændt = false;

let slider;
let imgMørkt;
let imgLyst;

function preload (){
  imgLyst = loadImage('lyst.jpg');
  imgMørkt = loadImage('mørkt.jpg');
}

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(255);

  //slider
  slider = createSlider(0, 7, 0);
  slider.position(248, 495);
  slider.style('width', '108px');

  frameRate(60);
}

function draw(){
  let sliderVal = slider.value();

  fill(addColorR, addColorG, addColorB);

  if (tændt == true) {
    timeVal = frameCount/4.5;
    addColorR =  255 - timeVal;
    addColorG = 200 - timeVal;
    addColorB = 148 - timeVal;
    //print(frameCount);
    if (sliderVal == 0 && frameCount >= 140) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    } else if (sliderVal == 1 && frameCount >= 225) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    } else if (sliderVal == 2 && frameCount >= 300) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    } else if (sliderVal == 3 && frameCount >= 375) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    } else if (sliderVal == 4 && frameCount >= 520) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    } else if (sliderVal == 5 && frameCount >= 595) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    } else if (sliderVal == 6 && frameCount >= 670) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    } else if (sliderVal == 7 && frameCount >= 745) {
      addColorR = 145;
      addColorG = 183;
      addColorB = 217;
      højde = 325;
    }
  } else {
    addColorR = 145;
    addColorG = 183;
    addColorB = 217;
    frameCount = 0;
  }

  beginShape();
  vertex(430, 550);
  vertex(180, 550);
  vertex(180,275);
  vertex(430,275);
  endShape(CLOSE);

  beginShape();
  vertex(180,275);
  vertex(490,100);
  vertex(720,100);
  vertex(510,275);
  endShape(CLOSE);

  beginShape();
  vertex(430, 550);
  vertex(430,275);
  vertex(720,100);
  vertex(720,375);
  endShape(CLOSE);

  fill(55);
  beginShape();
  vertex(280,250);
  vertex(510,120);
  vertex(550,120);
  vertex(320,250);
  endShape(CLOSE);

  beginShape();
  vertex(380,250);
  vertex(610,120);
  vertex(650,120);
  vertex(420,250);
  endShape(CLOSE);

  rect(305,320,10,110,10);
  if (mouseIsPressed && mouseX > 280 && mouseX < 330 && mouseY > 315 && mouseY < 435) {
    højde = mouseY;
    if (højde >= 425) {
      tændt = true;
    } else {
      tændt = false;
    }
  }

  ellipse(310,højde,40,40);

  noFill();
  rect(245,460,120,60);

  image(imgLyst, 255, 475, 50, 20);
  image(imgMørkt, 305, 475, 50, 20);

  print(mouseX, mouseY);
}
