var angle=0;
var vent;
function setup() {
  createCanvas(600,400);
  angleMode(DEGREES);
  frameRate(8);
}
function draw() {
background(179, 222, 252,65);
//calling my two functions
drawRotateThrobber(30,30);
writing();
}

function drawRotateThrobber(x,y){
//The rotating throbber
  push();
  noStroke();
  fill(83, 182, 252);
  //to make the ellipse rotate around the center of the canvas
  translate(300,200)
   rotate(angle);
   ellipse(x,y,22,22);
   //to make the ellipse rotate at a random speed
   angle=angle+random(50);
   pop();
   //reset the number of degrees to 0 if the angle is bigger than 360
   if (angle>360) {
     angle=0;
   }
 }

  function writing(){
  //The writing inside the throbber
    var words=["wait","vent","attendez","esperar","रुकिए","wag","等待 ","vänta"];
    if (angle == 0) {
      vent=random(words);
  }
  //An ellipse to cover up the fading text. If this is not here, the text will fade into the next word.
    fill(179, 222, 252);
    noStroke();
    ellipse(300,200,52,52);
  //The colors and style of the text
    fill(83, 182, 252);
    textSize(13);
    textStyle(BOLD);
    textAlign(CENTER, CENTER);
    text(vent,300,200);
 }
