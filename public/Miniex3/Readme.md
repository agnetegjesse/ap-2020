**<h2>Redesign and program an animated throbber</h2>**

RUNME: https://agnetegjesse.gitlab.io/ap-2020/Miniex3/



Folder with sketch- and index file: https://gitlab.com/agnetegjesse/ap-2020/-/tree/master/public/Miniex3


**Describe your throbber design, both conceptually and technically.**


**What is your sketch? What do you want to explore and/or express?**

My sketch is resembling an ordinary throbber which consists of ellipses rotating clockwise around the center of the canvas. In the middle of the throbber I have placed a text with the word “wait” in different languages. This text changes to a new word every time the throbber reaches 360 degrees, but the time interval between the shifting words is not consistent because I set the angle to increase with a random number from 0-50 at every frame. Therefore, it looks like the throbber is lacking and the viewer may think that his or her internet connection is slow because you expect a smoothly running throbber when your internet connection is working – the viewer don’t expect that this is the right outcome. “If one looks up the dictionary definition of the verb ‘throb’ it is defined as a strong and regular pulse rhythm that resonates with a throbber’s design and in regard to how it performs on the Internet today.” I chose to do this in order to express the actual non-smoothness and ruptures of the internet like we read about in the text “Throbber: Executing Micro-temporal Streams” by Winnie Soon. 

Another aim with my sketch is to explore different functions and make my own functions. Line 15 in my sketch file: function drawRotateThrobber(x,y) . I tried making a function with my own defined parameters to see if I could make it work. The point of this was that I would have liked to make more throbbers when the mouse was pressed at different places, but I couldn't get this to work.


**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?**

The syntax frameRate() is used to set a certain number of iterations pr. second, and I used it to make the ellipse move at a pace that I liked which meant that the viewer also has time to read the text in the middle and discover that the throbber is not moving smoothly.  

The var=angle is affecting the speed in which the ellipse is moving. This variable is increasing with a random number from 0-50, which makes it look like the movement of the ellipse is not moving smoothly.

The function background() is used as a time-related syntax because I have used an alpha-value. This value contributes to the time aspect in the program because it creates an illusion of fading which makes it even more clear to the viewer that the ellipse moves at different speeds because the level of opacity makes a fading trace of the ellipse’s movements. The more iterations, the more layers are put on top and this creates an illusion of fading.  


**How is time being constructed in computation (refer to both the reading materials and your process of coding)?**

“Computational time is highly related to speed of processing.” – p. 11. In the processing of data, a lot of things that are invisible to human perception takes place and time is not measured in minutes or seconds, but instead in when a job is processing and sending different states of the process back and forth. I have chosen to make it clear that data processing is not a smooth process that happen like one computer sending information to another. Instead it is messy, filled with ruptures and back and forth. 


**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?**

I have encountered a throbber in mutable cases, and when it happens, I always get impatient and assume that something is wrong with the internet. But still I don’t know why and how and what I can do besides just restarting the router or switching to 4G. The reason for why I think that there is nothing for me to do, is that a throbber makes it look like everything is working smoothly and I just have to wait. I think that a throbber communicates that something is in process and working with some data like it is supposed to. I would characterize the icon as an assurance of process and as an illusion of smoothness of the internet. 










![ScreenShot](miniex_3.PNG)

