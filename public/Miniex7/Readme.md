![ScreenShot](miniex7.PNG) 


RUNME: https://agnetegjesse.gitlab.io/ap-2020/Miniex7/ - the program must be run in firefox, because it uses microphone which I cannot get to work in any other browser. 

Folder with sketch and index file: https://gitlab.com/agnetegjesse/ap-2020/-/tree/master/public/Miniex7

**What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors? What's the role of rules and processes in your work?**

When I started working on my program, I first sat down with pen and paper and tried to figure out which rules I would like to implement and how the program should look.

I decided to implement the following rules:
- The movement of the circles depends on random values within ranges that I control 
- All circles must have the same x-value
- The size of the circles is random within a range 
- The color changes if any circle overlaps another
- The color of the circles depends on the mic input volume 

The program starts with five circles with same x-position and a y-position which has the same distance between the circles. The y-values move up and down in a random jitter which means that some of the circles get closer to each other at some point. The random jitter on the y-axis and noise-movement on the x-axis leaves a trace of ellipses in the same shape as the one being drawn right now. This is what is being generated in my program. When two (or more) circles eventually touch, they both change color into being bluer for as long as they touch. This gives the trace a new color and a new expression. 

The look of my program is also depending on environmental inputs in form of sound-input through the mic. If you are in a very noise place, the colors will have more red in them than if you are in a more silent environment.  This makes the color of the circles an aspect which will always change and never be predicted. 

The longer you run the program, the more “dense” the colors will get, which makes it harder to actually see the individual circles being drawn – unless you have a change in audio or if they overlap. If you let the program run for a very long time without any sound variations the background will be completely covered with the tracks of the circles. Still, the looks of the program in this situation is not completely possible to predict, because the circles can still overlap and thereby change the color. The dominant color will always be the default color, which is ranging between green or orange depending on the sounds of the environment. (if silent, the circles will be green, if noisy, the circles will be orange) 

**Draw upon the assigned reading(s), how does this mini-exericse help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?**

Before starting this mini-exercise I had a hard time understanding how randomness actually worked – especially what the difference between random() and noise() is. Now, I think that my program illustrates the difference very well. Noise is used on the x-axis and random is used on the y-axis. They both appear to generate random values, but actually they are both generating pseudorandom values based on their previous state. The difference between these two functions is that random has an equally big likeliness to choose every number within the defined range and does not follow a curtain pattern. It is even more random than natural randomness which eventually would form a pattern.

On the other hand, the noise function always produces an outcome between 0-1 where the randomness in the numbers is closer to normal distribution (where is collects around the center and less along the sides). This distribution around the center greatly affects the look of my program, where the background becomes covered much faster than the sides of the window. 

These two features are somethings that I have focused on displaying in my program. I have experimented a lot with the relation between something random and the aspect of control. A lot of the variables in the program have some kind of randomness to them, but in all cases, it is within a set and controlled range. When working with both noise() and random() you have a curtain expectation to what will happen if you pass on different arguments. This predictability in computed randomness is very different from natural randomness. 

**Some frustrations which you might help me understand:**

I decided to create a class for constructing my circle objects, because I really wanted to practice this and explore how objects can be used in if-statements and for loops. But I really had har hard time, figuring out which variables were available in different part of the program, and how I could change something inside the class (like my changeColor function which I found in Shiffman’s part two about object communication – the reference is inside my sketch). When can you change a variable in draw and make it affect something inside a specific object? (maybe the people who give feedback has an answer) 

Also, I couldn’t get the for loop which checks the distance between all the different objects to work if I tried writing it in the “normal” way for (let i=0, i<…., i++), but only when I wrote it like Shiffman showed in the video about object communication.
