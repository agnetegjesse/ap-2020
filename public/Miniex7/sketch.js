//my rules are:
// - The x-movement of the circles goes from a random starting point and uses noise()
// values as x-coordinate within the windowWidth. All circles have the same x-value.
// - The y-movements are random between a range of -5 to 5
// - The size of the circles is random within a range of 20-45 pixels
// - The color of the circles depends on the audio level
// - The color changes if any circle overlaps another

let mic;
let circles = [];

function setup (){
  createCanvas(windowWidth,windowHeight);
  background(255);
  frameRate(20);
  //mic is initialized and used to make color
  mic = new p5.AudioIn();
  mic.start();
  for (let i = 0; i < 5; i++) {
    //because I have given my object proporties, I am passing on the arguemnts
    let y = [i]*100+70;
    let size = floor(random(20,45));
    circles[i] = new Circle(y,size);
  }
}

function draw (){
  //I didn't understand this kind of loop, but I got it from Daniel Shiffman's following example: https://editor.p5js.org/codingtrain/sketches/7SjPmXN2
  for (let c of circles) {
    c.show(); // show and move every cicle
    c.move();

    let overlapping = false;
    for (let other of circles) {
      if (c !== other && c.circlesTouch(other)) {
        overlapping = true;
      }
    }
    if (overlapping) {
        c.changeColor(200); //if overlapping this value is the b value in rgb
     } else {
       c.changeColor(100); //if not overlapping this is the b value in rgb
    }
  }
}


//function where audio is mapped to a rgb value
function audio (){
  let vol = mic.getLevel();
  let col = map(vol,0,0.4,50,255);
  return (col);
}

//my circleClass
class Circle {
  constructor(y, size) {
    this.x;
    this.y = y;
    this.size = size;
    this.radius = this.size/2;
    this.vol = mic.getLevel();
    this.t = 0;
    this.speed = 5;
    this.colorB;
  }
  //https://www.youtube.com/watch?v=W1-ej3Wu5zg&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=33
  circlesTouch(other){
    //if the distance is less than the radius of this bubble and another bubble, they are overlapping
    let distance = dist(this.x,this.y,other.x,other.y);
    if (distance < this.radius + other.radius) {
      return true;
    }else {
      return false;
    }
  }
  //if overlapping, this is executed
  changeColor(b){
    this.colorB = b;
  }
  show(){
    noFill();
    //the mic.input is used as a part of the color
    stroke(audio(),150,this.colorB);
    ellipse(this.x,this.y,this.size,this.size);
  }
  move(){
    //setting up values for Noise
    this.t = this.t + 0.01;
    this.x = map(noise(this.t),0,1,0,width); //mapping the values to place within width
    this.y = this.y + random(-this.speed, this.speed); // the movements on the y-axis

//This was one of my attempts to stop the circles from exciting the window
    // if (this.y<this.r) {
    //   this.y = 40;
    // }else if (this.y>height-this.r) {
    //   this.y = height - 40;
    // }
  }
}
